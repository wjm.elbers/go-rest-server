package go_rest_server

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"net/http"
	"encoding/json"
	"github.com/rs/cors"
	"github.com/gorilla/mux"
)

type Server struct {
	hostname *string
	port *int
	verbose bool
	router *mux.Router
	logger *logrus.Logger
}

func NewServer(hostname *string, port *int, logger *logrus.Logger) (*Server) {
	return &Server{
		hostname: hostname,
		port: port,
		logger: logger,
		router: &mux.Router{},
	}
}

func (s *Server) RegisterRoute(path string, handler *RequestHandler, methods ...string) *Server {
	s.router.
		HandleFunc(path, func(w http.ResponseWriter, r *http.Request) {
			handler.
				SetLogger(s.logger).
				ProcessParameters(r).
				ProcessBody(r).
				DoWork().
				SendResponse(w)
		}).
		Methods(methods...)
	return s
}

func (s *Server) StartAndBlock() {
	//Configure CORS
	originsOk := []string{"*"}
	c := cors.New(cors.Options{
		AllowedOrigins: originsOk,
		AllowCredentials: true,
		AllowedMethods: []string{"GET", "POST", "PUT", "DELETE"},
	})

	//Start server
	address := fmt.Sprintf("%s:%d", *s.hostname, *s.port)
	fmt.Printf("Starting server: %s\n", address)
	s.logger.Fatal(http.ListenAndServe(address, c.Handler(s.router)))
}






func httpError(code int, msg string, err error) *HttpError {
	return &HttpError{
		err: err,
		msg: msg,
		code: code,
	}
}

func httpInternalServerError(err error) *HttpError {
	if err == nil {
		return nil
	}
	return httpError(http.StatusInternalServerError, err.Error(), err)
}

type HttpError struct {
	err error
	msg string
	code int
}

func (h *HttpError) Error() string {
	return h.err.Error()
}

type RequestHandler struct {
	logger *logrus.Logger
	err *HttpError
	body interface{}
	params map[string]string

	parameter_processor func(map[string]string) (map[string]string, *HttpError)
	worker func(map[string]string) (interface{}, *HttpError)
}

func NewRequestHandler() *RequestHandler {
	return &RequestHandler{logger: nil, params: nil, err: nil, body: nil}
}

func (r *RequestHandler) SetLogger(logger *logrus.Logger) *RequestHandler {
	r.logger = logger
	return r
}

func (r *RequestHandler) SetParameterProcessor(f func(map[string]string) (map[string]string, *HttpError)) *RequestHandler {
	r.parameter_processor = f
	return r
}

func (r *RequestHandler) SetWorker(f func(map[string]string) (interface{}, *HttpError)) *RequestHandler {
	r.worker = f
	return r
}


//Error results in bad request
func (r *RequestHandler) ProcessParameters(req *http.Request) *RequestHandler {
	if r.err != nil {
		return r
	}
	if r.parameter_processor == nil {
		r.params = nil
		return r
	}
	r.params, r.err = r.parameter_processor(mux.Vars(req))
	return r
}


//Error results in bad request
func (r *RequestHandler) ProcessBody(req *http.Request) *RequestHandler {
	if r.err != nil {
		return r
	}

	return r
}

//Error results in internal server error
func (r *RequestHandler) DoWork() *RequestHandler {
	if r.err != nil {
		return r
	}
	r.body, r.err = r.worker(r.params)
	return r
}

func (r *RequestHandler) SendResponse(w http.ResponseWriter) {
	w.Header().Add("Content-Type", "application/json")
	if r.err != nil {
		w.WriteHeader(r.err.code)
		json.NewEncoder(w).Encode(r.err.msg)
		fmt.Printf("Error: %s", r.err.Error())
	} else {
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(r.body)
	}
}