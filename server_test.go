package go_rest_server

import (
	"os"
	"testing"
	"github.com/sirupsen/logrus"
)

func TestServer(t *testing.T) {
	hostname := "localhost"
	port := 9999
	logger := getLogger()

	NewServer(&hostname, &port, logger).
		RegisterRoute("/test1", getTest1Handler(),"GET").
		RegisterRoute("/test2", getTest2Handler(), "POST")
}

func getLogger() *logrus.Logger {
	logrus_logger := logrus.New()
	logrus_logger.Out = os.Stdout
	logrus_logger.SetLevel(logrus.DebugLevel)
	return logrus_logger
}

func getTest1Handler() *RequestHandler {
	return NewRequestHandler().
		SetParameterProcessor(func(m map[string]string) (map[string]string, *HttpError) {

			return nil, nil
		}).
		SetWorker(func(m map[string]string) (interface{}, *HttpError) {
			return nil, nil
		})
}

func getTest2Handler() *RequestHandler {
	return NewRequestHandler().
		SetParameterProcessor(func(m map[string]string) (map[string]string, *HttpError) {

			return nil, nil
		}).
		SetWorker(func(m map[string]string) (interface{}, *HttpError) {
			return nil, nil
		})
}