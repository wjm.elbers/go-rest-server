module gitlab.com/wjm.elbers/go-rest-server

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/rs/cors v1.7.0
	github.com/sirupsen/logrus v1.8.1
)
